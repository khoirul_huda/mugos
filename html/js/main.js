window.addEventListener("load", function() { window.scrollTo(0, 1); });
$(document).ready(function() {

   var hblock = $('.header__block').outerHeight();
   var w = document.documentElement.clientWidth;
   // if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    if(w<768) {

        $('#js-menu, #js-menu-login').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            // console.log('menu');
            // $("body").toggleClass("js-menu--show");
        });
        $('#js-menu').sidr({
            side: 'left',
            name: 'sidr-menu',
            displace: false,
            onOpenEnd: function() {
                // alert('s');
                $('.menu__main').toggleClass('active');
            },
            onClose: function() {
                $('.menu__main').toggleClass('active');
            }
        });
        $('#js-menu-login').sidr({
            side: 'right',
            name: 'sidr-account',
            displace: false,
            onOpenEnd: function() {
                // alert('s');
                $('.menu__account').toggleClass('active');
            },
            onClose: function() {
                $('.menu__account').toggleClass('active');
            }
        });
        $('.js-menu-close, #overlay').click(function(e) {
            e.preventDefault();
            $.sidr('close', 'sidr-menu');
            $.sidr('close', 'sidr-account');
        });
        $('.is--stacked--bottom').click(function(e) {
            e.preventDefault();
            $('.chatbox__form').removeClass('is--stacked--bottom');
            // $('html, body').animate({
            //     scrollTop: $('.chatbox__form').offset().top + 100
            // }, 'fast');
            $('.form__input--chat').focus();
            console.log($('.chatbox__form').offset().top);
        });

        // chatbox
        // $("#chat").scrollTop($("#chat")[0].scrollHeight);

        var hheader = $('.header').outerHeight();
        var hchatboxfrm = $('.chatbox__form').outerHeight();

        if($(".js-chatbox--pos").length>0) {
            var jcb = $(".js-chatbox--pos");
            jcb.css('margin-bottom', hchatboxfrm);
        }
        $(window).scroll(function () {
            var scrollpos = $(window).scrollTop();
            if($(".js-chatbox--pos").length>0) {
                var jcb = $(".js-chatbox--pos");
                var ctsdpos = jcb.offset().top - $(window).height();
                var vdposs = $(".js-video-stacked").offset().top - hheader;
                var vdpos = $(".js-video-stacked").offset().top - $(window).height();
                // chatbox form
                if(scrollpos > ctsdpos+hchatboxfrm) {
                    $(".chatbox__form").removeClass("is--stacked--bottom");
                    $(".js-chatbox--pos").removeAttr('style');
                }else{
                    $(".chatbox__form").addClass("is--stacked--bottom");
                    $(".js-chatbox--pos").css('margin-bottom', hchatboxfrm);
                }

                if((scrollpos > vdposs) && (scrollpos < $(".js-chatbox--pos").offset().top - hheader - $('.video').outerHeight())) {
                // if((scrollpos > vdposs) && (scrollpos < ctsdpos+hchatboxfrm+$('.video').outerHeight())) {
                // if((scrollpos > vdposs) && (scrollpos < vdposs+$('.chatbox__title').outerHeight())) {
                    $('.video').addClass("is--stacked");
                    $('.video').css({
                        top: hheader,
                        margin: '0px'
                    });
                    $('.js-video-stacked').css('padding-top', $('.video').outerHeight());
                }else{
                    $('.video').removeClass("is--stacked");
                    $('.video, .js-video-stacked').removeAttr('style');
                }
            }
            console.log(vdposs);
            console.log(scrollpos);
            // console.log(vdpos);
            console.log(ctsdpos);
            // console.log($(".js-chatbox--pos").offset().top - $(window).height() + $('.video').outerHeight() + hheader + hchatboxfrm);
            console.log($(".js-chatbox--pos").offset().top - hheader - $('.video').outerHeight());
        });

        /* we need this only on touch devices */
        // if (Modernizr.touch) {
            /* cache dom references */ 
            // console.log('e');
            // var $body = jQuery('body'); 

            /* bind events */
            // $(document)
            // .on('focus', 'input', function() {
            //     $body.addClass('fixfixed');
            //     $('.chatbox__form').css({
            //         position:'relative',
            //         // bottom: 'auto',
            //         // top:window.pageYOffset+'px'
            //     });
            // })
            // .on('blur', 'input', function() {
            //     $body.removeClass('fixfixed');
            //     $('.chatbox__form').removeAttr('style');
            // });
        // } 


    } else {

        // desktop
        $(window).scroll(function () {
            var scrollpos = $(window).scrollTop();
            // back to top
            if(scrollpos > hblock) {
                $("body").addClass("is--scroll");
            }else{
                $("body").removeClass("is--scroll");
            }
        });

        $('.js-login').magnificPopup({
            type:'inline',
            // delegate: 'a',
            removalDelay: 300, //delay removal by X to allow out-animation
            mainClass: 'mfp-fade',
            midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });

        $('.js-menu-account').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            // console.log('menu');
            $(".nav--account__logged__menu").toggleClass("mfp-hide");
        });

        if($("#chat").length>0) {
            $("#chat").scrollTop($("#chat")[0].scrollHeight);
        }

    }


    $('.js-trending__now').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        // fade: true,
        cssEase: 'ease'
    });

    $(window).scroll(function () {
        var scrollpos = $(window).scrollTop();
        // back to top
        if(scrollpos > $(window).height()) {
            $(".js-back-btn").removeClass("display--hide");
        }else{
            $(".js-back-btn").addClass("display--hide");
        }
    });

    $(document).click(function() {
        console.log('menu hide');
        $(".nav--account__logged__menu").addClass("mfp-hide");
        $(".schedule__tab__row").addClass("schedule__tab__row--hide");
    });

    // menu schedule
    $('.js-tab-setting').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        // console.log('menu');
        $(".schedule__tab__row").toggleClass("schedule__tab__row--hide");
    });

    // pop up
    $('.js-schedule__link').magnificPopup({
        type:'inline',
        // delegate: 'a',
        removalDelay: 300, //delay removal by X to allow out-animation
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    // general
    $('.js-popup').magnificPopup({
        type:'inline',
        // delegate: 'a',
        removalDelay: 300, //delay removal by X to allow out-animation
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        closeOnBgClick: false,
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
    $('.js-popup-close').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $.magnificPopup.close();
    });

    // back top
    $('#js-back-top').click(function(e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 'slow');
    });

    // tabs
    $('[data-type="tabs"]').each(function(){

        var $active, $content, $links = $(this).find('a');
        $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
        //$active = $active.parent();
        var $activeNameClass = $active.attr('class');
        $active.addClass($activeNameClass+'--active');
        $content = $($active.attr('href'));

        $links.not($active).each(function () {
            $($(this).attr('href')).hide();
        });

        $(this).on('click', 'a', function(e){
            $active.removeClass($activeNameClass+'--active');
            $content.hide();

            $active = $(this);
            $content = $($(this).attr('href'));

            $active.addClass($activeNameClass+'--active');
            $content.show();

            e.preventDefault();
        });
    });

});
