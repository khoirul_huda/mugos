window.addEventListener("load", function() { window.scrollTo(0, 1); });
$(document).ready(function() {

   var hblock = $('.header__block').outerHeight();
   var w = document.documentElement.clientWidth;
   // if( /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    if(w<768) {

        $('#js-menu, #js-menu-login').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            // console.log('menu');
            // $("body").toggleClass("js-menu--show");
        });
        $('#js-menu').sidr({
            side: 'left',
            name: 'sidr-menu',
            displace: false,
            onOpenEnd: function() {
                // alert('s');
                $('.menu__main').toggleClass('active');
            },
            onClose: function() {
                $('.menu__main').toggleClass('active');
            }
        });
        $('#js-menu-login').sidr({
            side: 'right',
            name: 'sidr-account',
            displace: false,
            onOpenEnd: function() {
                // alert('s');
                $('.menu__account').toggleClass('active');
            },
            onClose: function() {
                $('.menu__account').toggleClass('active');
            }
        });
        $('.js-menu-close, #overlay').click(function(e) {
            e.preventDefault();
            $.sidr('close', 'sidr-menu');
            $.sidr('close', 'sidr-account');
        });
        // $('.is--stacked--bottom').click(function(e) {
        //     e.preventDefault();
        //     $('.chatbox__form').removeClass('is--stacked--bottom');
        //     // $('html, body').animate({
        //     //     scrollTop: $('.chatbox__form').offset().top + 100
        //     // }, 'fast');
        //     $('.form__input--chat').focus();
        //     console.log($('.chatbox__form').offset().top);
        // });

        // chatbox
        // $("#chat").scrollTop($("#chat")[0].scrollHeight);

        var hheader = $('.header').outerHeight();
        var hchatboxfrm = $('.chatbox__form').outerHeight();

        if($(".js-chatbox--pos").length>0) {
            var jcb = $(".js-chatbox--pos");
            jcb.css('margin-bottom', hchatboxfrm);
        }
        $(window).scroll(function () {
            var scrollpos = $(window).scrollTop();
            if($(".js-chatbox--pos").length>0) {
                var jcb = $(".js-chatbox--pos");
                var ctsdpos = jcb.offset().top - $(window).height();
                var vdposs = $(".js-video-stacked").offset().top - hheader;
                var vdpos = $(".js-video-stacked").offset().top - $(window).height();
                // console.log(vdposs);
                // chatbox form
                if(scrollpos > ctsdpos+hchatboxfrm) {
                    $(".chatbox__form").removeClass("is--stacked--bottom");
                    $(".js-chatbox--pos").removeAttr('style');
                }else{
                    $(".chatbox__form").addClass("is--stacked--bottom");
                    $(".js-chatbox--pos").css('margin-bottom', hchatboxfrm);
                }

                if((scrollpos > vdposs) && (scrollpos < $(".js-chatbox--pos").offset().top - hheader - $('.video').outerHeight())) {
                // if((scrollpos > vdposs) && (scrollpos < ctsdpos+hchatboxfrm+$('.video').outerHeight())) {
                // if((scrollpos > vdposs) && (scrollpos < vdposs+$('.chatbox__title').outerHeight())) {
                    if(!$('.video').hasClass('is--stacked')){
                        $(window).scrollTop(205);
                        $('.js-video-stacked').css('padding-top', $('.video').outerHeight());
                        $('.video').addClass("is--stacked");
                        $('.video').css({
                            top: hheader,
                            margin: '0px'
                        });
                        $('.share-small').css("display", "none");
                    }
                }else{
                    if($('.video').hasClass('is--stacked')){
                        $('.video').removeClass("is--stacked");
                        $('.video, .js-video-stacked').removeAttr('style');
                    }
                }
            }
            // console.log('----s');
            // console.log(vdposs);
            // console.log(scrollpos);
            // console.log(vdpos);
            // console.log(ctsdpos);
            // console.log($(".js-chatbox--pos").offset().top - $(window).height() + $('.video').outerHeight() + hheader + hchatboxfrm);
            // console.log($(".js-chatbox--pos").offset().top - hheader - $('.video').outerHeight());
            // console.log('----e');
        });

        /* we need this only on touch devices */
        // if (Modernizr.touch) {
            /* cache dom references */ 
            // console.log('e');
            // var $body = jQuery('body'); 

            /* bind events */
            // $(document)
            // .on('focus', 'input', function() {
            //     $body.addClass('fixfixed');
            //     $('.chatbox__form').css({
            //         position:'relative',
            //         // bottom: 'auto',
            //         // top:window.pageYOffset+'px'
            //     });
            // })
            // .on('blur', 'input', function() {
            //     $body.removeClass('fixfixed');
            //     $('.chatbox__form').removeAttr('style');
            // });
        // } 


    } else {

        // desktop
        $(window).scroll(function () {
            var scrollpos = $(window).scrollTop();
            // back to top
            if(scrollpos > hblock) {
                $("body").addClass("is--scroll");
            }else{
                $("body").removeClass("is--scroll");
            }
        });

        $('.js-login').magnificPopup({
            type:'inline',
            // delegate: 'a',
            removalDelay: 300, //delay removal by X to allow out-animation
            mainClass: 'mfp-fade',
            midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });

        $('.js-menu-account').click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            // console.log('menu');
            $(".nav--account__logged__menu").toggleClass("mfp-hide");
        });

        if($("#chat").length>0) {
            $("#chat").scrollTop($("#chat")[0].scrollHeight);
        }

    }


    $('.js-trending__now').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        // fade: true,
        cssEase: 'ease'
    });

    $(window).scroll(function () {
        var scrollpos = $(window).scrollTop();
        // back to top
        if(scrollpos > $('.footer').offset().top - $(window).height() - 200) {
            $(".js-back-btn").removeClass("display--hide");
        }else{
            $(".js-back-btn").addClass("display--hide");
        }
    });

    $(document).click(function() {
        //console.log('menu hide');
        $(".nav--account__logged__menu").addClass("mfp-hide");
        $(".schedule__tab__row").addClass("schedule__tab__row--hide");
    });

    // menu schedule
    $('.js-tab-setting').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        // console.log('menu');
        $(".schedule__tab__row").toggleClass("schedule__tab__row--hide");
    });

    // pop up
    $('.js-schedule__link').magnificPopup({
        type:'inline',
        // delegate: 'a',
        removalDelay: 300, //delay removal by X to allow out-animation
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    // general
    $('.js-popup').magnificPopup({
        type:'inline',
        // delegate: 'a',
        removalDelay: 300, //delay removal by X to allow out-animation
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        closeOnBgClick: false,
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
    $('.js-popup-close').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $.magnificPopup.close();
    });

    // back top
    $('#js-back-top').click(function(e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 'slow');
    });

    // tabs
    $('[data-type="tabs"]').each(function(){

        var $active, $content, $links = $(this).find('a');
        $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
        //$active = $active.parent();
        var $activeNameClass = $active.attr('class');
        $active.addClass($activeNameClass+'--active');
        $content = $($active.attr('href'));

        $links.not($active).each(function () {
            $($(this).attr('href')).hide();
        });

        $(this).on('click', 'a', function(e){
            $active.removeClass($activeNameClass+'--active');
            $content.hide();

            $active = $(this);
            $content = $($(this).attr('href'));

            $active.addClass($activeNameClass+'--active');
            $content.show();

            e.preventDefault();
        });
    });

    // gift
    $('.js-gift-btn').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        //console.log('gift');
        $(this).toggleClass("gift__btn--active");
        $(".js-gift-table").toggleClass("gift__table--hide");

        // matiin emoji
        $(".js-emoji-table").addClass("emoji__table--hide");
        $(".js-emoji-btn").removeClass("emoji__btn--active");
    });

    // emoji
    $('.js-emoji-btn').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        //console.log('emoji');
        $(this).toggleClass("emoji__btn--active");
        $(".js-emoji-table").toggleClass("emoji__table--hide");

        // matiin emoji
        $(".js-gift-table").addClass("gift__table--hide");
        $(".js-gift-btn").removeClass("gift__btn--active");
    });

    // emoji select
    $('.emoji__item__select').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        //console.log($(this).data('emoji'));

        // insert into cursor pos
        var cursorPos = $('.form__input--chat').prop('selectionStart');
        var v = $('.form__input--chat').val();
        var textBefore = v.substring(0,  cursorPos);
        var textAfter  = v.substring(cursorPos, v.length);

        $('.form__input--chat').val(textBefore + $(this).data('emoji') + textAfter);

    });

    // like
    $('.schedule__like__button').click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        console.log('vh');
        $(this).toggleClass('schedule__liked');
    });

    // tooltips
    $('[data-type="tooltips"]').each(function() {
        var sf = $(this).data('text');
        $(this).append('<div class="tooltips"><span>' + sf + '</span></div>');
        $(this).on('mouseover', function(e) {
            e.preventDefault();
            $(this).find('.tooltips').toggleClass('tooltips--show');
        });
        $(this).on('mouseout', function(e) {
            e.preventDefault();
            $(this).find('.tooltips').toggleClass('tooltips--show');
        });
    });

    // avatar
    // $('.js-chatbox-avatar').mouseover(function(e) {
    //     e.stopPropagation();
    //     e.preventDefault();
    //     $(this).find('.chatbox__avatar').toggleClass('chatbox__avatar--hide');
    // });


    /* Muhamad Ali */
    var anchor = document.querySelectorAll('body')[0];
    var docking = anchor.getAttribute('data-pid');
    function isempty(obj){
        if(typeof obj !== 'undefined'){if(obj.val() != '' && obj.val() != null){return false;}}
        return true;
    }
    function validate(obj, empty){
        if(empty){if(!obj.hasClass('form--error')){obj.addClass('form--error');obj.append('<label>You can\'t leave this empty.</label>');}}
        else{obj.removeClass('form--error');obj.find('label').remove();}
    }
    if(docking == 'register'){
       $('#input-name').focusout(function(){validate($('#input-name'),isempty($('#input-name').find('input')));});
       $('#input-email').focusout(function(){validate($('#input-email'),isempty($('#input-email').find('input')));});
       $('#input-password').focusout(function(){validate($('#input-password'),isempty($('#input-password').find('input')));});
       $('#input-repassword').focusout(function(){validate($('#input-repassword'),isempty($('#input-repassword').find('input')));});
       $('#input-mobile').focusout(function(){validate($('#input-mobile'),isempty($('#input-mobile').find('input')));});
       $('form#register').submit(function(){
            if( !$("#input-terms").find('input').is(':checked') || isempty($('#input-name').find('input')) || isempty($('#input-email').find('input')) || isempty($('#input-password').find('input')) || isempty($('#input-repassword').find('input')) || isempty($('#input-mobile').find('input')) ){
                validate($('#input-name'),isempty($('#input-name').find('input')));
                validate($('#input-email'),isempty($('#input-email').find('input')));
                validate($('#input-password'),isempty($('#input-password').find('input')));
                validate($('#input-repassword'),isempty($('#input-repassword').find('input')));
                validate($('#input-mobile'),isempty($('#input-mobile').find('input')));
                return false;
            }
       });
    }
    if(docking == 'contact'){
        $('#input-name').focusout(function(){validate($('#input-name'),isempty($('#input-name').find('input')));});
        $('#input-email').focusout(function(){validate($('#input-email'),isempty($('#input-email').find('input')));});
        $('#input-message').focusout(function(){validate($('#input-message'),isempty($('#input-message').find('textarea')));});
        $('form#contact').submit(function(){
            if( isempty($('#input-name').find('input')) || isempty($('#input-email').find('input')) || isempty($('#input-message').find('textarea')) ){
                validate($('#input-name'),isempty($('#input-name').find('input')));
                validate($('#input-email'),isempty($('#input-email').find('input')));
                validate($('#input-message'),isempty($('#input-message').find('textarea')));
                return false;
            }
        });
    }
    if(docking == 'profile'){
        $("#profile-edit-button").click(function(){
            $('#profile').hide();
            $('#profile-edit').show();
            return false;
        });
        $("#profile-cancle-button").click(function(){
            $('#profile').show();
            $('#profile-edit').hide();
            return false;
        });    
    }

    // Share drop-down
    $('.js-share-small').on('click', function() {
        var status = $('.share-small').css("display");
        if (status == "none") {
            $('.share-small').css("display", "inline-block");
        } else {
            $('.share-small').css("display", "none");
        }
    });
    $('.close-share-small').on('click', function(){ $('.share-small').css("display", "none"); });

    /* update May 2019 */
    $('.js-schedule-slider').slick({
        autoplay: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 5000,
        // fade: true,
        cssEase: 'ease',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                centerMode: false,
                slidesToShow: 1
                }
            },
        ]            
    });
});
